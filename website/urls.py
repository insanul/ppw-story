
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('story_5.urls')),
    path('portofolio/', include('story_7.urls')),
    path('search_book/', include('story_8.urls')),
]
