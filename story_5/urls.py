from django.contrib import admin
from django.urls import *
from .views import *

app_name='story_5'
urlpatterns = [
    path('', index, name='index'),
    path('contact/', contact, name='contact'),
    path('schedule/', schedule, name='schedule'),
    path('schedule/add_schedule/', add_schedule, name='add_schedule'),
    path('deleteList/<int:identity>/', deleteList, name='deleteList'),
]
