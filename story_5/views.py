from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from datetime import datetime, date
from .forms import *
from .models import *
from django.urls import reverse

# Create your views here.
def index(request):
	response = {}
	return render(request, 'index.html', response)

def contact(request):
	response = {}
	return render(request, 'contact.html', response)

def schedule(request): 
    schd = Schedule.objects.all()
    return render(request, 'schedule.html', {'table' : schd})


def add_schedule(request):
    if request.method == 'POST':
        form = Add_Schedule(request.POST)
        if form.is_valid():
            schd = Schedule.objects.all()
            nama = form.cleaned_data["nama_kegiatan"]
            hari = form.cleaned_data["hari"]
            tanggal = form.cleaned_data["tanggal"]
            jam = form.cleaned_data["jam"]
            tempat = form.cleaned_data["tempat"]
            kategori = form.cleaned_data["kategori"]

            jadwal = Schedule(nama_kegiatan = nama, hari= hari, tanggal=tanggal, jam=jam, tempat=tempat, kategori=kategori)
            jadwal.save()
            return render(request, 'schedule.html', {'table' : schd})
            # return HttpResponseRedirect(reverse('schedule'))
        else:
            return HttpResponseRedirect('add_schedule')
    else:
        form = Add_Schedule()
        return render(request, 'add_schedule.html', {'form' : form}) 
    

def deleteList(request, identity):
    Schedule.objects.get(id = identity).delete()
    # return HttpResponseRedirect(reverse('schedule'))

    # Schedule.objects.get(id = identity).delete()
    # form = Add_Schedule(request.POST)
    # return HttpResponseRedirect(reverse('schedule')) 
    schd = Schedule.objects.all()
    return render(request, 'schedule.html', {'table' : schd})

