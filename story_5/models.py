from django.utils import timezone
from datetime import datetime, date
from django.db import models

class Schedule(models.Model):
    nama_kegiatan = models.CharField(max_length=30)
    hari = models.CharField(max_length=30)
    tanggal = models.DateField() #datefield karena model tidak ada datetimefield
    jam = models.TimeField()
    tempat = models.CharField(max_length=30)
    kategori = models.CharField(max_length=30)

    def __str__(self):
        return self.nama_kegiatan
