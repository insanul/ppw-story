from django import forms
from .models import Schedule

class Add_Schedule(forms.Form):

    attrs_nama_kegiatan = {
        'id': 'nama',
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Kerja',
    }

    attrs_hari = {
        'id': 'hari',
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Senin',
    }

    attrs_tanggal = {
        'id': 'tanggal',
        'type': 'date',
        'class': 'form-control',
    }

    attrs_jam = {
        'id': 'jam',
        'type': 'time',
        'class': 'form-control',
    }

    attrs_tempat = {
        'id': 'tempat',
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Universitas Indonesia'
    }

    attrs_kategori = {
        'id': 'kategori',
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Belajar PPW'
    }


    nama_kegiatan = forms.CharField(max_length=30, widget = forms.TextInput(attrs=attrs_nama_kegiatan)) #CharField, TextInput
    hari = forms.CharField(max_length=30, widget = forms.TextInput(attrs=attrs_hari)) #DateField, TextInput
    tanggal = forms.DateTimeField(widget = forms.DateInput(attrs=attrs_tanggal)) #DateTimeField, DateInput
    jam = forms.TimeField(widget = forms.TimeInput(attrs=attrs_jam)) #TimeField, TimeInput
    tempat = forms.CharField(max_length=30, widget = forms.TextInput(attrs=attrs_tempat)) #CharField, TextInput
    kategori = forms.CharField(max_length=30, widget = forms.TextInput(attrs=attrs_kategori)) #CharField, TextInput

