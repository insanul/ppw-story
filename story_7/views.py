from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from datetime import datetime, date
from django.urls import reverse

def portofolio(request):
	response = {}
	return render(request, 'portofolio.html', response)
