from django.test import TestCase, Client
from django.urls import resolve
from .views import *


class Test_Story_7(TestCase):
	def test_url_portofolio_isexist(self):
		response = Client().get('')
		self.assertEqual(response.status_code, 200)

	def test_uses_portofolio_page_view(self): #cek url memanggil fungsi yang mana
		handler = resolve('/portofolio/') #nama argumen pertama di urls project
		self.assertEqual(handler.func, portofolio) #nama fungsi di views

	def test_uses_portofolio_page_template(self): #cek views merender html
		response = Client().get('/portofolio/') #nama argumen pertama di urls project
		self.assertTemplateUsed(response, 'portofolio.html')
