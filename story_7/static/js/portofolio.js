$(document).ready(function(){

	$("#button1").click(function(){
		if($(this).hasClass("active")){
			$(this).removeClass("active");
			$(this).text("Dark Mode");
			$('#box').css('background-color', '#C3DEDB');
			$('body').css('background-color', '#FCF8ED');
			$('.header').css('background-color', '#F18E68');
			$('.footer').css('background-color', '#F18E68');
			$('#judul').css('color', 'black');
			$('#text-ftr').css('color', 'black');
		}

		else{
			$(this).addClass("active");
			$(this).text("Light Mode");
			$('#box').css('background-color', '#585858');
			$('body').css('background-color', '#262525');
			$('.header').css('background-color', '#585858');
			$('.footer').css('background-color', '#585858');
			$('#judul').css('color', '#4BA469');
			$('#text-ftr').css('color', '#4BA469');
		}
	});

	$(".set > a").click(function() {
		if ($(this).hasClass("active")) {
			$(this).removeClass("active");
			$(this).siblings(".content").slideUp(200);
		} 
		else {
			$(".set > a").removeClass("active");
			$(this).addClass("active");
			$(".content").slideUp(200);
			$(this).siblings(".content").slideDown(200);
		}
	});
});