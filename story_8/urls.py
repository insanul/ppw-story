from django.contrib import admin
from django.urls import *
from .views import *

app_name='story_8'
urlpatterns = [
    path('', search_book, name='search_book'),
    path('search/<title>', search, name='search'),
]