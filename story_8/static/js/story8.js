$(document).ready(function(){
	search("thesaurus");
});

function readInput(){
	var title = $('#input').val();
	$('#input').val("");
	search(title);
}

function search(title){
	$("tbody").empty();
	$.ajax({
		url: "search/" + title,
		success: function(list){
			if(list.totalItems != 0){
				list.items.forEach(function(book){
					$("tbody").append(`
						<tr>
							<td><img src="${book.volumeInfo.imageLinks.smallThumbnail}"></td>
							<td>${book.volumeInfo.title}</td>
							<td>${book.volumeInfo.authors}</td>
						</tr>
					`);
				});
			} else{
				$("tbody").append(`
					<tr>
						<td colspan=6 class="text-center">No search results found</td>
					</tr>
				`);
			}
		}
	});
}

