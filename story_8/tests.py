from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import *


class Test_Story_8(TestCase):
	def test_url_portofolio_isexist(self):
		response = Client().get('')
		self.assertEqual(response.status_code, 200)

	def test_uses_portofolio_page_view(self): #cek url memanggil fungsi yang mana
		handler = resolve('/search_book/') #nama argumen pertama di urls project
		self.assertEqual(handler.func, search_book) #nama fungsi di views

	def test_uses_portofolio_page_template(self): #cek views merender html
		response = Client().get('/search_book/') #nama argumen pertama di urls project
		self.assertTemplateUsed(response, 'search_book.html')