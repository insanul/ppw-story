from django.shortcuts import render
from django.http import JsonResponse
import requests
import json

def search_book(request):
	response = {}
	return render(request, 'search_book.html', response)

def search(request, title):
	response = requests.get('https://www.googleapis.com/books/v1/volumes?q=intitle:' + title)
	return JsonResponse(response.json())